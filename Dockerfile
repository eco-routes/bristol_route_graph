FROM ubuntu:16.04
MAINTAINER HAL24K "docker@hal24k.com"

ARG PIP_TRUSTED_HOST
ARG PIP_EXTRA_INDEX_URL

# Some general updates 
RUN apt-get update -y && \
    apt-get install -y python3-pip python3 python3-tk git libgeos-dev

# Install requirements and copy app files
COPY requirements.txt /tmp/requirements.txt


WORKDIR /app
COPY . /app
RUN pip3 install --upgrade pip
RUN pip3 install \
        --trusted-host ${PIP_TRUSTED_HOST} \
        --extra-index-url ${PIP_EXTRA_INDEX_URL} \
        --no-cache-dir \
        -r /app/requirements.txt


# apt-get and system utilities
#RUN apt-get update -y && apt-get install -y --no-install-recommends \
#			apt-utils \
#			apt-transport-https \
#			build-essential \
#			curl \
#			debconf-utils \
#			gcc \
#			g++-5 \
#			software-properties-common \
#			wget



# install additional utilities
#RUN apt-get update -y && apt-get install -y --no-install-recommends \
#                gettext \
#                nano \
#                vim


# Sample for python:3 with miniconda
#FROM python:3
#
##Set env for conda
#ENV CONDA_DIR="/opt/conda"
#ENV PATH="$CONDA_DIR/bin:$PATH"
#
## Install conda
#RUN CONDA_VERSION="4.3.14" && \
#    CONDA_MD5_CHECKSUM="fc6fc37479e3e3fcf3f9ba52cae98991" && \
#    apt-get install -y wget ca-certificates bash && \
#    mkdir -p "$CONDA_DIR" && \
#    wget "http://repo.continuum.io/miniconda/Miniconda3-${CONDA_VERSION}-Linux-x86_64.sh" -O miniconda.sh && \
#    echo "$CONDA_MD5_CHECKSUM  miniconda.sh" | md5sum -c && \
#    bash miniconda.sh -f -b -p "$CONDA_DIR" && \
#    echo "export PATH=$CONDA_DIR/bin:\$PATH" > /etc/profile.d/conda.sh && \
#    rm miniconda.sh && \
#    #conda update --all --yes && \
#    conda config --set auto_update_conda False && \
#    rm -r "$CONDA_DIR/pkgs/" && \
#    mkdir -p "$CONDA_DIR/locks" && \
#    chmod 777 "$CONDA_DIR/locks"
#
#CMD ["python3"]

ENTRYPOINT [ "python3" ]
CMD ["src/runserver.py"]
