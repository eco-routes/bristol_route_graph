
"""
This script runs the Flask application using a development server.
"""

from os import environ
from cdx_api import app

if __name__ == '__main__':
    HOST = environ.get('SERVER_HOST', '0.0.0.0')
    try:
        PORT = int(environ.get('SERVER_PORT', '19888'))
    except ValueError:
        PORT = 19888
    app.run(HOST, PORT, debug = True)
