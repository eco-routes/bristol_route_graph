import unittest
from cdx_api import app


class BaseTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def tearDown(self):
        self.app = None


class GeneralApiTestCase(BaseTestCase):

            def test_root(self):
                r = self.app.get('/')
                self.assertEqual(200, r.status_code)
                self.assertEqual(b'Set up complete', r.data)

            def test_status(self):
                r = self.app.get('/status')
                self.assertEqual(200, r.status_code)
                self.assertEqual(b'status', r.data)