from shapely.geometry import LineString, Point
import numpy as np
import pandas as pd
import osmnx as ox
import geopandas as gpd
from fiona.crs import from_epsg
from shapely.geometry import Point
from googlemaps import googlemaps
import configparser

def get_api_key(name_api_key, config_file='config.ini'):
    """
    Read api key from a local credentials file
    Parameters
    ----------
    name_api_key: string
        Name of the api key to look for
    config_file: string
        location of the config file
    Returns
    -------
    string
        api key from file
    """
    config = configparser.ConfigParser()
    config.read(config_file)
    
    #with open(config_file) as f:
        #for line in f:
            #if name_api_key in line:
                #return next(f, '').split(':')[1].strip()
    
    return config['API_KEYS'][name_api_key]

def str_to_int(s):
    """Converts a string to int if possible"""
    try: 
        return int(s)
    except ValueError:
        return s

def get_nearest_nodes_to_point(G, point, num_points=10, method='haversine', return_dist=False):
    """
    Return the graph nodes nearest to some specified (lat, lng) or (y, x) point,
    and optionally the distance between the node and the point. This function
    can use either a haversine or euclidean distance calculator.
    Parameters
    ----------
    G : networkx multidigraph
        Graph created by osmnx
    point : tuple
        The (lat, lng) or (y, x) point for which we will find the nearest node
        in the graph
    method : str {'haversine', 'euclidean'}
        Which method to use for calculating distances to find nearest node.
        If 'haversine', graph nodes' coordinates must be in units of decimal
        degrees. If 'euclidean', graph nodes' coordinates must be projected.
    num_points : int
        Number of closest points to return
    return_dist : bool
        Optionally also return the distance (in meters if haversine, or graph
        node coordinate units if euclidean) between the point and the nearest
        node.
    Returns
    -------
    int or tuple of (int, float)
        Nearest node ID or optionally a tuple of (node ID, dist), where dist is
        the distance (in meters if haversine, or graph node coordinate units
        if euclidean) between the point and nearest node
    """
    
    if not G or (G.number_of_nodes() == 0):
        raise ValueError('G argument must be not be empty or should contain at least one node')

    # dump graph node coordinates into a pandas dataframe indexed by node id
    # with x and y columns
    coords = np.array([[node, data['x'], data['y']] for node, data in G.nodes(data=True)])
    df = pd.DataFrame(coords, columns=['node', 'x', 'y']).set_index('node')

    # add columns to the dataframe representing the (constant) coordinates of
    # the reference point
    df['reference_y'] = point[0]
    df['reference_x'] = point[1]

    # calculate the distance between each node and the reference point
    if method == 'haversine':
        # calculate distance vector using haversine (ie, for
        # spherical lat-long geometries)
        distances = ox.utils.great_circle_vec(lat1=df['reference_y'].astype(float),
                                              lng1=df['reference_x'].astype(float),
                                              lat2=df['y'].astype(float),
                                              lng2=df['x'].astype(float))

    elif method == 'euclidean':
        # calculate distance vector using euclidean distances (ie, for projected
        # planar geometries)
        distances = ox.utils.euclidean_dist_vec(y1=df['reference_y'].astype(float),
                                                x1=df['reference_x'].astype(float),
                                                y2=df['y'].astype(float),
                                                x2=df['x'].astype(float))

    else:
        raise ValueError('method argument must be either "haversine" or "euclidean"')

    # nearest node's ID is the index label of the minimum distance
    distances.sort_values(inplace=True)
    distances.index = distances.index.map(lambda x: str_to_int(x))

    # if caller requested return_dist, return distance between the point and the
    # nearest node as well
    if return_dist:
        return list(distances.index[:num_points]), list(distances.values[:num_points])
    else:
        return list(distances.index[:num_points])

def add_node_to_graph(G, point, name):
    """
    Add a node to the graph at the specified position by inserting it on the closest edge
    Parameters
    ----------
    G : networkx multidigraph
        The graph the node is added to, changed in place
    point : tuple
        The (lat, lng) or (y, x) point for node to be added
        in the graph
    name : str
        Name of new node
    Returns
    -------
    """
    
    pointP = Point(point[::-1])
    
    # Get all nearby edges to the point
    node_list = get_nearest_nodes_to_point(G, point, num_points=10)
    edge_list = [list(G.in_edges(node))+list(G.out_edges(node)) for node in node_list]
    edge_list = list(set([edge for sublist in edge_list for edge in sublist]))
    # Get the linestring of all nearby edges
    linestr_list = []
    proj_list = []
    dist_list = []
    for edge in edge_list:
        edge_dict = G.get_edge_data(*edge)[0]
        if 'geometry' not in edge_dict:
            point_u = Point((G.nodes[edge[0]]['x'], G.nodes[edge[0]]['y']))
            point_v = Point((G.nodes[edge[1]]['x'], G.nodes[edge[1]]['y']))
            line = LineString([point_u, point_v])
        else:
            line = edge_dict['geometry']
        proj = line.interpolate(line.project(pointP))
        dist = ox.utils.great_circle_vec(point[0], point[1], proj.y, proj.x)
        linestr_list.append(line)
        proj_list.append(proj)
        dist_list.append(dist)
    # Add all edges within 30m and the closest
    to_append = list(np.where(np.array(dist_list)<30)[0])
    if len(to_append)==0:
        to_append = [np.argmin(np.array(dist_list))]
    
    for edge_id in to_append:
        # Calculate where to split the edge
        edge_pts = list(zip(*linestr_list[edge_id].xy))
        _, split_id = min((val, idx) for (idx, val) in enumerate([Point(pt).distance(pointP) 
                                                                  for pt in edge_pts]))
        # Calculate how far this is along
        split_frac = linestr_list[edge_id].project(pointP, normalized=True)

        # Add new edges
        edge = edge_list[edge_id]
        edge_dict = G.get_edge_data(*edge)[0].copy()
        if 'length' not in edge_dict:
            length = ox.utils.great_circle_vec(G.nodes[edge[0]]['y'], G.nodes[edge[0]]['x'], 
                                               G.nodes[edge[1]]['y'], G.nodes[edge[1]]['x'])
        else:
            length = edge_dict['length']

        edge_dict['geometry'] = LineString(edge_pts[:split_id+1]+[(proj_list[edge_id].x,proj_list[edge_id].y),point[::-1]])
        edge_dict['length'] = length*split_frac+dist_list[edge_id]
        G.add_edge(edge[0], name, key=0)
        G[edge[0]][name][0].update(edge_dict.copy())

        edge_dict['geometry'] = LineString([point[::-1], (proj_list[edge_id].x,proj_list[edge_id].y)]+edge_pts[split_id:])
        edge_dict['length'] = length*(1-split_frac)+dist_list[edge_id]
        G.add_edge(name, edge[1], key=0)
        G[name][edge[1]][0].update(edge_dict.copy())

        # Set node parameters
        G.node[name]['osmid'] = None
        G.node[name]['x'] = point[1]
        G.node[name]['y'] = point[0]
        
def convert_point_list(pt_list, old_epsg, new_epsg):
    """
    Convert a list of x,y tuples between coordinate systems
    Parameters
    ----------
    pt_list: list of (x, y) tuples
        List of points to convert
    old_epsg: int
        epsg of old coordinate system
    new_epsg: int
        epsg of new coordinate system
    Returns
    -------
    list of (x, y) tuples
        List of converted points
    """
    # Make conversion via geodataframe
    gdf = gpd.GeoDataFrame({'geometry':[Point(pt) for pt in pt_list]})
    gdf.crs = from_epsg(old_epsg)
    gdf.to_crs(epsg=new_epsg, inplace=True)
    
    return [(pt.coords[0]) for pt in gdf['geometry']]

def straight_line_dist(pointA, pointB):
    """
    Calculate the straight line distance between two points
    Parameters
    ----------
    pointA: (x, y) tuple
        First point (in metres)
    pointB: (x, y) tuple
        Second point (in metres)
    Returns
    -------
    float
        Distance between them
    """    
    return np.sqrt((pointA[0]-pointB[0])**2 + (pointA[1]-pointB[1])**2)

def get_elevation(G, path, local_epsg=27700, resolution=100.):
    """
    Get the elevation profile of an osmnx path
    Parameters
    ----------
    G : networkx multidigraph
        Graph created by osmnx
    path: list
        List of edges that make up a continuous path in graph G
    local_epsg: int
        Local coordinate system for the graph
    resolution: float
        The maximum resolution (in metres) to sample the path at
    Returns
    -------
    
    """    
    lines = []
    for i in range(0, len(path)-1):
        u = path[i]
        v = path[i+1]
        data = G.get_edge_data(u, v)[0]
        if 'geometry' in data:
            # if it has a geometry attribute (a list of line segments), add them
            # to the list of lines to plot
            xs, ys = data['geometry'].xy
            lines.append(list(zip(xs, ys)))
        else:
            # if it doesn't have a geometry attribute, the edge is a straight
            # line from node to node
            x1 = G.nodes[u]['x']
            y1 = G.nodes[u]['y']
            x2 = G.nodes[v]['x']
            y2 = G.nodes[v]['y']
            line = [(x1, y1), (x2, y2)]
            lines.append(line)
    # Make one list of lon/lat
    lines = [pt for line in lines for pt in line]
    # Convert to coords in metres
    lines = convert_point_list(lines, old_epsg=4326, new_epsg=local_epsg)
    
    # Downsample the line to the specified resolution
    simple_lines = [lines[0]]
    dist = 0
    for i in range(1, len(lines)-1):
        dist+=(straight_line_dist(lines[i-1], lines[i]))
        if dist > resolution:
            simple_lines.append(lines[i])
            dist = 0
    simple_lines.append(lines[-1])
    
    # Convert back to lon lat
    simple_lines = convert_point_list(simple_lines, old_epsg=local_epsg, new_epsg=4326)
    
    # Connect to google maps client
    api_key = get_api_key('googlemaps')
    gmaps = googlemaps.Client(key=api_key)
    # Get the elevation along path - need to flip from lon lat to lat lon
    elevation = gmaps.elevation([pt[::-1] for pt in simple_lines])
    
    return elevation
